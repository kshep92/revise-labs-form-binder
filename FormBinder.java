package util;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import play.db.ebean.Model;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Kevin Sheppard on 7/28/2014.
 */
public class FormBinder {
    private ObjectMapper mapper = new ObjectMapper();

    /**
     * Read the field values from one object and write it on to
     * this object.
     * @param source The object from which to read all the information
     * @param instanceType The Class of object that must be returned
     * @param <T> Java generic Type
     * @return A an object with all of its values set from another object
     */
    public <T> T readFrom(Object source, Class<T> instanceType) {
        Object returnObject;
        try {
            returnObject = instanceType.newInstance();
            mapper.assign(source, returnObject);
            return instanceType.cast(returnObject);
        }
        catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Copy field values from this object to another object.
     * @param target The object to copy all the field values to.
     */
    public void writeTo(Object target) {
        mapper.assign(this, target);
    }

    private class ObjectMapper {
        private List<String> sourceFieldNames = new ArrayList<>();
        private List<String> targetFieldNames = new ArrayList<>();

        public void assign(Object sourceObject, Object targetObject) {
            if(targetObject instanceof Model) {
                BeanUtils.copyProperties(sourceObject, targetObject, getNullPropertyNames(sourceObject));
            } else {
                Class source = sourceObject.getClass();
                Class target = targetObject.getClass();

                for(Field f: source.getFields()) {
                    sourceFieldNames.add(f.getName());
                }
                for(Field f: target.getFields()) {
                    targetFieldNames.add(f.getName());
                }

                try {
                    for(String s: targetFieldNames) {
                        if(sourceFieldNames.indexOf(s)  > -1) {
                            Field sourceField = source.getField(s);
                            Field targetField = target.getField(s);
                            Object value = sourceField.get(sourceObject);
                            targetField.set(targetObject, value);
                        }
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * Helper method to get the null values from a Bean
     * @param source The object from which to read all the information
     * @return a String[] of the names of the fields whose values are null
     */
    public static String[] getNullPropertyNames (Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for(java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
}

